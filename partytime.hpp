#ifndef PARTYTIME
#define PARTYTIME

#include <stddef.h>
#include <stdint.h>

/* Compiler specific */

#if defined(__clang__) || defined(__GNUC__)
#define PARTYTIME_INLINE static inline __attribute__((__always_inline__, __unused__))
#define PARTYTIME_LIKELY(x) __builtin_expect(!!(x), 1)
#define PARTYTIME_UNLIKELY(x) __builtin_expect(!!(x), 0)

#elif defined(_MSC_VER)
#include "intrin.h"
#define PARTYTIME_INLINE static inline __forceinline
#define PARTYTIME_LIKELY(x) (x)
#define PARTYTIME_UNLIKELY(x) (x)

#else
#define PARTYTIME_INLINE static inline
#define PARTYTIME_LIKELY(x) (x)
#define PARTYTIME_UNLIKELY(x) (x)

#endif

// TODOS:
// revise atomics and set the correct ordering
// canceling is done by setting task->fn = nullptr atomically

namespace partytime {

/* Atomics */

namespace atomic {

template<typename T>
PARTYTIME_INLINE T load(T* ptr) {
    return __atomic_load_n(ptr, __ATOMIC_RELAXED);
}

template<typename T>
PARTYTIME_INLINE T load_acquire(T* ptr) {
    return __atomic_load_n(ptr, __ATOMIC_ACQUIRE);
}

template<typename T, typename U>
PARTYTIME_INLINE void store(T* ptr, U val) {
    return __atomic_store_n(ptr, val, __ATOMIC_RELAXED);
}

template<typename T, typename U>
PARTYTIME_INLINE void store_release(T* ptr, U val) {
    return __atomic_store_n(ptr, val, __ATOMIC_RELEASE);
}

template<typename T, typename U>
PARTYTIME_INLINE T xchg(T* ptr, U val) {
    return __atomic_exchange_n(ptr, val, __ATOMIC_RELAXED);
}

template<typename T, typename U>
PARTYTIME_INLINE T xchg_acqrel(T* ptr, U val) {
    return __atomic_exchange_n(ptr, val, __ATOMIC_ACQ_REL);
}

template<typename T, typename U, typename V>
PARTYTIME_INLINE bool cmpxchg(T* ptr, U* expected, V desired) {
    return __atomic_compare_exchange_n(ptr,
        expected,
        desired,
        false,
        __ATOMIC_RELAXED,
        __ATOMIC_RELAXED);
}

template<typename T, typename U, typename V>
PARTYTIME_INLINE bool cmpxchg_acqrel(T* ptr, U* expected, V desired) {
    return __atomic_compare_exchange_n(ptr,
        expected,
        desired,
        false,
        __ATOMIC_ACQ_REL,
        __ATOMIC_ACQUIRE);
}

template<typename T, typename V>
PARTYTIME_INLINE T xadd(T* ptr, V val) {
    return __atomic_fetch_add(ptr, val, __ATOMIC_RELAXED);
}

template<typename T, typename V>
PARTYTIME_INLINE T add(T* ptr, V val) {
    return __atomic_add_fetch(ptr, val, __ATOMIC_RELAXED);
}

} // namespace atomic

struct ThreadImpl {
    using Thread = pthread_t;
    using WaitObject = pthread_cond_t;
    using Mutex = pthread_mutex_t;

    PARTYTIME_INLINE void spawn_thread(Thread* thread, void* (*func)(void*), void* arg) {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

        pthread_create(thread, &attr, func, arg);

        pthread_attr_destroy(&attr);
    }

    PARTYTIME_INLINE void stop_thread(Thread thread) {
        pthread_cancel(thread);
    }

    PARTYTIME_INLINE void init_mutex(Mutex* mutex) {
        pthread_mutex_init(mutex, nullptr);
    }

    PARTYTIME_INLINE void destroy_mutex(Mutex* mutex) {
        pthread_mutex_destroy(mutex);
    }

    PARTYTIME_INLINE void init_wait_obj(WaitObject* wait_obj) {
        pthread_cond_init(wait_obj, nullptr);
    }

    PARTYTIME_INLINE void destroy_wait_obj(WaitObject* wait_obj) {
        pthread_cond_destroy(wait_obj);
    }

    PARTYTIME_INLINE void wake(WaitObject* wait_obj) {
        pthread_cond_signal(wait_obj);
    }

    PARTYTIME_INLINE void wake_all(WaitObject* wait_obj) {
        pthread_cond_broadcast(wait_obj);
    }

    PARTYTIME_INLINE void wait(WaitObject* wait_obj, Mutex* mutex) {
        pthread_mutex_lock(mutex);
        pthread_cond_wait(wait_obj, mutex);
    }
};

template<typename T>
struct RingBuffer {
    size_t head;
    size_t tail;
    size_t mask;

    T data[];
};

namespace ring_buffer {

// size must be 2^n
template<typename T>
void init(RingBuffer<T>* ring, size_t size) {
    ring->head = 0;
    ring->tail = 0;
    ring->mask = size - 1;
}

template<typename T>
bool is_empty(RingBuffer<T>* ring) {
    size_t head = atomic::load(&ring->head) & ring->mask;
    size_t tail = atomic::load(&ring->tail) & ring->mask;

    return head == tail;
}

template<typename T>
void push(RingBuffer<T>* ring, T item) {
    size_t idx = atomic::xadd(&ring->tail, 1) & ring->mask;
    ring->data[idx] = item;
}

template<typename T>
T pop(RingBuffer<T>* ring) {
    size_t idx = atomic::xadd(&ring->head, 1) & ring->mask;
    return ring->data[idx];

//    size_t idx = ring->head & ring->mask;
//    ring->head++;
//
//    return ring->data[idx];
}

template<typename T>
T pop_from_tail(RingBuffer<T>* ring) {
    while(true) {
        size_t head = ring->head;
        size_t tail = atomic::load(&ring->tail);

        if(head == tail) {
            return 0;
        }

        bool success = atomic::cmpxchg(&ring->tail, tail, tail - 1);
        if(success) {
            return ring->data[tail & ring->mask];
        }
    }
}

} // namespace ring_buffer


using task_fn = void(void*);

struct Task {
    task_fn* fn;
    void* arg;
};

template<typename ThreadImpl = ThreadImpl>
struct Pool;

template<typename ThreadImpl>
struct Worker {
    using Thread = typename ThreadImpl::Thread;

    struct Pool<ThreadImpl>* pool;
    Thread thread;
    size_t idx;

    RingBuffer<Task*> ring;
};

template<typename ThreadImpl>
struct Pool {
    using WaitObject = typename ThreadImpl::WaitObject;

    size_t thread_nr;
    size_t ring_size;
    size_t worker_size;

    size_t push_idx;

    size_t active_threads;
    WaitObject sleep_wait_obj;

    size_t join_counter;
    WaitObject join_wait_obj;

    bool is_running;
    bool is_joined;

    Worker<ThreadImpl> workers[];
};

#define GET_WORKER(pool, idx) \
    reinterpret_cast<Worker<ThreadImpl>*>( \
        reinterpret_cast<char*>((pool)->workers) + (idx) * (pool)->worker_size \
    )

#define FOR_EACH_WORKER(pool, worker) \
    for(Worker<ThreadImpl>* (worker) = (pool)->workers; \
        (worker) != reinterpret_cast<Worker<ThreadImpl>*>(reinterpret_cast<char*>((pool)->workers) + pool->thread_nr * (pool)->worker_size); \
        (worker) = reinterpret_cast<Worker<ThreadImpl>*>(reinterpret_cast<char*>(worker) + (pool)->worker_size))

template<typename ThreadImpl = ThreadImpl>
Pool<ThreadImpl>* create(
    size_t thread_nr,
    size_t ring_size
) {
    size_t worker_size = sizeof(Worker<ThreadImpl>) + ring_size * sizeof(Task);

    auto pool = reinterpret_cast<Pool<ThreadImpl>*>(
        malloc(
            sizeof(Pool<partytime::ThreadImpl>)
            + thread_nr * worker_size
        )
    );

    pool->thread_nr = thread_nr;
    pool->ring_size = ring_size;
    pool->worker_size = worker_size;
    pool->push_idx = 0;

    pool->active_threads = 0;
    ThreadImpl::init_wait_obj(&pool->sleep_wait_obj);

    pool->join_counter = 0;
    ThreadImpl::init_wait_obj(&pool->join_wait_obj);

    pool->is_running = false;
    pool->is_joined = false;

    size_t i = 0;
    FOR_EACH_WORKER(pool, worker) {
        ring_buffer::init(&worker->ring, ring_size);
        worker->idx = i;
        worker->pool = pool;

        i++;
    }

    return pool;
}

template<typename ThreadImpl>
static void destroy(Pool<ThreadImpl>* pool) {
    ThreadImpl::destroy_wait_obj(pool->sleep_wait_obj);
    ThreadImpl::destroy_wait_obj(pool->join_wait_obj);

    free(pool);
}

template<typename ThreadImpl>
static void push_task(Pool<ThreadImpl>* pool, Task* task) {
    size_t push_idx = atomic::xadd(&pool->push_idx, 1) % pool->thread_nr;

    Worker<ThreadImpl>* worker = GET_WORKER(pool, push_idx);
    ring_buffer::push(&worker->ring, task);

//    if (pool->is_running && pool->active_threads < pool->thread_nr && !pool->is_waking) {
//        ThreadImpl::wake(&pool->sleep_wait_obj);
//    }
}

template<typename ThreadImpl>
static void* task_wrapper(void* arg) {
    using Mutex = typename ThreadImpl::Mutex;

    auto worker = reinterpret_cast<Worker<ThreadImpl>*>(arg);
    Pool<ThreadImpl>* pool = worker->pool;

    size_t worker_size = sizeof(Worker<ThreadImpl>) + pool->ring_size * sizeof(Task);

    RingBuffer<Task*>* ring = &worker->ring;
    size_t worker_idx = worker->idx;

    atomic::add(&pool->active_threads, 1);

    Mutex wait_mutex;
    ThreadImpl::init_mutex(&wait_mutex);

    while (true) {
next_task:;
        size_t idx = atomic::xadd(&ring->head, 1);
        size_t tail = atomic::load(&ring->tail);

        // if idx is admissible run the task and continue
        if(idx < tail) {
            Task* task = ring->data[idx & ring->mask];
            task->fn(task->arg);
            continue;
        }

        // else buffer empty -> reset ring->head
        atomic::add(&ring->head, -1);

        // try to steal tasks
        size_t target_idx = (worker_idx + 1) % pool->thread_nr;
        while (target_idx != worker_idx) {
            RingBuffer<Task*>* other_ring = &GET_WORKER(pool, target_idx)->ring;

            size_t stolen_idx = atomic::add(&other_ring->tail, -1);
            size_t stolen_head = atomic::load(&other_ring->head);

            if(stolen_head <= stolen_idx) {
                Task* task = other_ring->data[stolen_idx & other_ring->mask];
                task->fn(task->arg);
                goto next_task;
            }

            atomic::add(&other_ring->tail, 1);
            target_idx = (target_idx + 1) % pool->thread_nr;
        }

        if (!atomic::load_acquire(&pool->is_running)) {
            break;
        }

        atomic::add(&pool->active_threads, -1);

        // sleep if no task to steal are available
        ThreadImpl::wait(&pool->sleep_wait_obj, &wait_mutex);
        atomic::add(&pool->active_threads, 1);
    }

    ThreadImpl::destroy_mutex(&wait_mutex);

    // update atomic counter and if it's the last one wake join
    size_t joined_threads = atomic::add(&pool->join_counter, 1);
    if(joined_threads != pool->thread_nr) {
        return nullptr;
    }

    while (!atomic::load_acquire(&pool->is_joined)) {
        ThreadImpl::wake(&pool->join_wait_obj);
    }

    return nullptr;
}

template<typename ThreadImpl>
static void start(Pool<ThreadImpl>* pool) {
    atomic::store(&pool->is_running, true);

    size_t i = 0;
    FOR_EACH_WORKER(pool, worker) {
        ThreadImpl::spawn_thread(&worker->thread,
            &task_wrapper<ThreadImpl>,
            worker);

        i++;
    }
}

template<typename ThreadImpl>
static void stop(Pool<ThreadImpl>* pool) {
    atomic::store(&pool->is_running, false);
    while (atomic::load(&pool->active_threads) != pool->thread_nr) {
        ThreadImpl::wake_all(&pool->sleep_wait_obj);
    }
}

//template<typename ThreadImpl>
//static void stop_now(Pool<ThreadImpl>* pool) {
//    for (size_t i = 0; i < pool->thread_nr; i++) {
//        ThreadImpl::stop_thread(&pool->threads[i]);
//    }
//}

template<typename ThreadImpl>
static void join(Pool<ThreadImpl>* pool) {
    using Mutex = typename ThreadImpl::Mutex;

    Mutex join_mutex;
    ThreadImpl::init_mutex(&join_mutex);

    stop(pool);
    ThreadImpl::wait(&pool->join_wait_obj, &join_mutex);
    atomic::store_release(&pool->is_joined, true);

    ThreadImpl::destroy_mutex(&join_mutex);
}

}; // namespace partytime

#endif