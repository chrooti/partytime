#include <chrono>
#include <iostream>
#include <stdlib.h>

#include <sched.h>

#include "../partytime.hpp"

static constexpr unsigned int spawned_threads = 24;
static constexpr unsigned int spawned_tasks = 7500000;
static constexpr unsigned int respawned_tasks = 7500000;
static constexpr unsigned int total_tasks = spawned_tasks + respawned_tasks;

static unsigned int respawned_tasks_count = 0;

static constexpr size_t thread_nr = 24;
static unsigned int results_per_cpu[thread_nr] = {0};
static char* total_results;
partytime::Task* tasks;

partytime::Pool<>* pool;
// 62495508
// 79568147
// 231111808
// 629240403

//  81945111
//  97006028 nanoseconds
//  149372436 nanoseconds

//  617281069 nanoseconds
//  647642027 nanoseconds
void task_example(void* arg) {
    auto num = reinterpret_cast<size_t>(arg);

    unsigned int cpu, node;
    getcpu(&cpu, &node);

    results_per_cpu[cpu]++;
    total_results[num]++;
    unsigned int respawned_count = partytime::atomic::load_acquire(&respawned_tasks_count);
    if(respawned_count < respawned_tasks) {
        unsigned int idx = partytime::atomic::xadd(&respawned_tasks_count, 1);
        tasks[spawned_tasks + idx] = {
            .fn = &task_example,
            .arg = reinterpret_cast<void*>(spawned_tasks + idx),
//            .next = nullptr,
        };

        partytime::push_task(pool, &tasks[spawned_tasks + idx]);
    }
}

int main() {
    std::ios_base::sync_with_stdio(false);

    // ring size should be nearest higher po2
    pool = partytime::create(spawned_threads, 8388608);

    total_results = reinterpret_cast<char*>(malloc(total_tasks * sizeof(char)));
    if (total_results == nullptr) {
        return -1;
    }

    tasks =
        reinterpret_cast<partytime::Task*>(malloc(total_tasks * sizeof(partytime::Task)));
    if (tasks == nullptr) {
        return -1;
    }

    for (unsigned int i = 0; i < spawned_tasks; i++) {
        tasks[i] = {
            .fn = &task_example,
            .arg = reinterpret_cast<void*>(i),
//            .next = nullptr,
        };

        partytime::push_task(pool, &tasks[i]);
    }

    auto start = std ::chrono::high_resolution_clock::now();

    partytime::start(pool);
    partytime::join(pool);

    auto end = std::chrono::high_resolution_clock::now();

    std::cerr << "Runtime: "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()
              << " nanoseconds" << std::endl;

//    for (unsigned int i = 0; i < thread_nr; i++) {
//        std::cerr << "CPU " << i << " executed around " << results_per_cpu[i] << " tasks\n";
//    }

    unsigned int should_be_equal_to_spawned_tasks = 0;
    for (unsigned int i = 0; i < (spawned_tasks + respawned_tasks); i++) {
        should_be_equal_to_spawned_tasks += total_results[i];
    }

    std::cerr << "Spawned threads: " << spawned_threads << "\n"
              << "Spawned tasks: " << (spawned_tasks + respawned_tasks) << "\n"
              << "Should be equal to spawned tasks: " << should_be_equal_to_spawned_tasks << "\n";
}
